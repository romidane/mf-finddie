
document.addEventListener('deviceready', function(e){

    var game = App.initialize();
    game.init();

   	//google auth
    googleplaygame.auth(function(){
        console.log('sucess loged in to google');
    },function(e){
    	console.log(e.googlePlayError.errorString);

    });


    // //Handle back button press
    // document.addEventListener("backbutton", function(e){
    //     gameSound.stop();
    //     gameSound.release();
    // 	navigator.app.exitApp();
    // }, false);
    //
    function setOverrideBackbutton(){
        if (typeof device != "undefined" && device.platform == "Android"){
            navigator.app.overrideBackbutton(true);
        }
    }

    setOverrideBackbutton();


    document.addEventListener("backbutton", onBackKeyDown, false);

    function onBackKeyDown() {
        gameSound.stop();
        gameSound.release();
        navigator.app.exitApp();
    }

    document.addEventListener("pause", function(){
        if(!game.soundMuted){
            game.soundMuted = true;
        }
        gameSound.stop();
    }, false);

    document.addEventListener("resume", function(){
        if(!game.soundMuted){
            game.soundMuted = false;
        } else {
            game.soundMuted = true;
        }
        gameSound.play();
    }, false);




}, false);