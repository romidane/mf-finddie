var gameSound;
var url = document.getElementById('gameMainSong').src;

var loop = function (status) {
  loop.isMuted = false;
  if (status === Media.MEDIA_STOPPED) {

    if(loop.isMuted) {
      muteGameSound();
    }

    gameSound.play();
  }
};

function startGameSound(status){
    if (!gameSound)
      gameSound = new Media(url, null, null, loop);

    loop.isMuted = status;

    if(status)
      muteGameSound();

    gameSound.play();
}

function stopGameSound(){
  if(gameSound) {
    gameSound.pause();
    gameSound.seekTo(0);
  }
}

function muteGameSound(){
  gameSound.setVolume('0.0');
}

function unmuteGameSound(){
  gameSound.setVolume('1.0');
}



function generateSound(file, soundStatus){

var audio = new Media(file, null, null, function(status){
  if (status === Media.MEDIA_STOPPED) {
    audio.release();
  }
});

if(soundStatus)
  return;
else
  return audio.play();
}

var facebookShare = function (data) {
if (!window.cordova) {
    var appId = prompt("Enter FB Application ID", "");
    facebookConnectPlugin.browserInit(appId);
}
facebookConnectPlugin.login( ["email"], 
    function (response) {
      var options = {
        method: "feed",
        name:"I just scored "+ data +" points on Morning Fresh Finddie",
        picture:"http://msmorningfreshng.com/mf-finddie/public/images/share-icon.png",
        caption:"Do you think you got what it takes to beat it?"
      };


      facebookConnectPlugin.showDialog( options, null, null);
    },
    function (response) { alert('failed') });
};


// var showLeaderBoard = function(){
//   var data = {
//     leaderboardId: 'CgkIxuvVzboMEAIQAQ'
//   };

//   googleplaygame.showLeaderboard(data, function(){
//     console.log('yay!');
//   }, function(){
//       alert('woops something went wrong!');
//   });
// };

