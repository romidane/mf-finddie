  var App = (function(App){
    'use strict';

    var cardTypes = {
      'miss-mf': {
        type: 'face'
      },
      'miss-mf2': {
        type: 'face'
      },
      'bottle-g': {
        type: 'bottle'
      },
      'bottle-y': {
        type: 'bottle'
      },
      'bottle-b1': {
        type: 'bottle'
      },
      'bottle-b2': {
        type: 'bottle'
      },
      'plate': {
        type: 'plate'
      }
    };

    var messages = {
      'face': 'Find and touch the <span class="red">HAPPY</span> face. Be quick, time is running out!',
      'bottle': 'Find the <span class="red">new</span> Morning Fresh bottle. Be quick, time is running out!',
      'plate': 'Find the squeaky <span class="red">clean</span> plate. Be quick, time is running out!'
    };

    var querySelector = document.querySelector.bind(document);

    var bestScore = localStorage['mf-bestscore'];

    var holder = document.getElementById('game');
    var style = window.getComputedStyle(holder, null);
    var holderWidth = holder.offsetWidth;
    var holderHeight =  window.innerHeight;


    if(window.innerHeight < 480 || window.innerHeight > 768) {
      holder.style.height = '768px';
    }else {
      holder.style.height = window.innerHeight + 'px';
    }


    //holder.style.height = (window.innerHeight < 640)? 640 + 'px' : window.innerHeight + 'px';


    if(!bestScore)
        localStorage['mf-bestscore'] = '0';


    /* Helper functions */
    function extend(target, source){
      if(!source) return;

      for (var prop in source) {
        if (source.hasOwnProperty(prop) && !target[prop]) {
          target[prop] = source[prop];
        }
      }
    }

    // Randomely select card type
    function randomCardType(cardsObj, mix){
        var cards = Object.keys(cardsObj);
      var randomCard = cards[Math.floor(Math.random() * cards.length)];
      var selectedType;

      if(!mix){
        return randomCard;
      } else {
        var selectedType = cardsObj[randomCard].type;
        var cardmix = [];

        for (var prop in cardsObj) {
          if (cardsObj.hasOwnProperty(prop)) {
            if (cardsObj[prop].type === selectedType) {
              cardmix.push(String(prop));
            }
          }
        }
        return { cardselection: cardmix, type: selectedType};
      }
    }


    //Matches selector
    function matchesSelector(elem,selector){
        var matchesList = ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector'];
        var matches = ''
        for (var i = matchesList.length - 1; i >= 0; i--) {
            if(matchesList[i] in elem){
                matches =  matchesList[i];
                break;
            }
        }

        return elem[matches](selector);

    }

    function touchCapable(){
      return ('ontouchstart' in document.documentElement )? 'touchstart': 'click'
    }


    function Game(config){

      this.baseTimer = config.baseTimer || 30;
      this.timer = 0;
      this.score = 0;
      this.reduceTimeBy = 5000;
      this.cache = {};
      this.cleanUpInter = null;

      extend(this, config);

    }


    Object.defineProperties(Game.prototype, {

      //start game
      init: {
        value: function(){
          if(!this.gameContainer)
            throw new Error('Please provide a container for your game');

          var self = this;

          //startGameSound
          startGameSound(this.soundMuted);

          //Add event for in game listener
          self.gameContainer.addEventListener(touchCapable(), function(e){

            e.preventDefault();
            // Remove selected class on all cards
            self.removeErrorClass(e.target.parentNode);
            //target card
            if( matchesSelector(e.target,'li[data-std]') )
              return self.evaluateSelection(e.target);

            if( matchesSelector(e.target, '.info-circle.restart') )
                return self.restart();

            if( matchesSelector(e.target, '.info-circle.play') )
                return self.startGame();

            if( matchesSelector(e.target, '.info-circle.home') )
                return self.backHome();

            if( matchesSelector(e.target, '.info-circle.sound') )
              self.toggleSound();

            if( matchesSelector(e.target, '.info-circle.share') )
              self.shareFacebook();

            if( matchesSelector(e.target, '.info-circle.playservices') )
              self.showLeaderBoard();

            if( matchesSelector(e.target, 'a'))
              self.handleLink(e);

          }, false);

          self.gameContainer.addEventListener(touchCapable(), function(e){
            var noTarget = !e.target.classList.contains('level') && !e.target.classList.contains('timer') && !e.target.classList.contains('logos')
            if( matchesSelector(e.target,'.info-circle') && noTarget){
              generateSound(self.gameSounds.querySelector('.buttonSound').src, self.soundMuted);
            }
          });

        },
        enumerable: true
      },

      startGame: {
        value: function(){
            this.restart();
            this.gameStartScreen.setAttribute('data-show', false);
          this.gameScreen.setAttribute('data-show', true);
        }
      },

      backHome: {
        value: function(){

          clearInterval(this.cleanUpInter);

            var cardHolder = this.gameCardContainer.querySelector('ul');
          var style = this.gameCardContainer.style;
          this.gameOverContainer.setAttribute('data-gmover', false);
          this.gameScreen.setAttribute('data-show', false);
            this.gameStartScreen.setAttribute('data-show', true);


          startGameSound(this.soundMuted);

            //Remove all the lis
            cardHolder.innerHTML = '';

            //Reset level
            this.score = 0;

            //Cleanup
            this.cleanUp();

            //return to start
          var transform = 'translateX(0px)';
          style.MozTransform = transform;
          style.WebkitTransform = transform;
          style.OTransform = transform;

                //Reset timer
                this.baseTimer = this.flatBaseTime;
            }

      },

      // Restart Game
      restart: {
        value: function(){
            var cardHolder = this.gameCardContainer.querySelector('ul');
          var style = this.gameCardContainer.style;

          //Restart sound
            startGameSound(this.soundMuted);

            //Remove all the lis
            cardHolder.innerHTML = '';

            //Reset level
            this.score = 0;

          //isSong playing?
          if(this.gameMainTune.paused)
            this.gameMainTune.play();

            //Cleanup
            this.cleanUp();

            //populate game
            this.populateGame();

            //return to start
            var transform = 'translateX(0px)';
          style.MozTransform = transform;
          style.WebkitTransform = transform;
          style.OTransform = transform;

            //hide the gameover screen
          this.gameOverContainer.setAttribute('data-gmover', false);

          this.baseTimer = this.flatBaseTime;

            //Start timer
          this.startTimer();
        },
        enumerable: true
      },

      handleLink: {
        value: function(e){
          var href = e.target.href;
          window.open(href, "_system");
        }
      },

      //generate level
      generateLevel: {
        //target, name, numCol, numCards, dataflip
        value: function(data){
            //Create holder ul
            var li = document.createElement('li');
              li.className = 'holder';
          li.style.width = this.width + 'px';

              //Create holder ul
              var ul = document.createElement('ul');
              ul.setAttribute('data-colnum', data.colNum);

              //Generate document frag
              var docFrag = document.createDocumentFragment();

              //Random  number for smileFace
              var isSmile = Math.floor(Math.random() * data.cardNum);

              for(var x = 0; x < data.cardNum; x++) {
                    var li2 = document.createElement("li");
            //give them a class name
            li2.className = '';

            if(typeof data.cardType === 'string') {
              li2.setAttribute('data-type', data.cardType);
            } else {
              li2.setAttribute('data-type', data.cardType.cardselection[Math.floor(Math.random() * data.cardType.cardselection.length)] );
            }

            if(data.flipCards && Math.floor(Math.random() * data.cardNum) === x)
              li2.setAttribute('data-flip', 'std-' + (isSmile === x ? 'n': 'y'));

                    if(x === isSmile)
                        li2.setAttribute('data-std', 'n');
                    else
                        li2.setAttribute('data-std', 'y');

                    docFrag.appendChild(li2);
              }

              ul.appendChild(docFrag);
              li.appendChild(ul);

              data.target.appendChild(li);
            //Display message if not already shown
              this.displayInstructions(data.cardType);

        }
      },

      displayInstructions: {
        value: function(type){
          var cardType;
          if(typeof type === 'string')
              cardType = cardTypes[type].type;
          else
            cardType = type.type;

            this.displayInstructions.cache = {};

            if(this.cache[cardType]) return false;

                this.cache[cardType] = 1;
                this.gameInstructions.innerHTML = messages[cardType];
                this.gameInstructions.setAttribute('data-instructions-visible', true);

          return true;
        }
      },

      removeInstructions: {
        value: function(){
            this.gameInstructions.innerHTML = '';
                this.gameInstructions.setAttribute('data-instructions-visible', false);
        }

      },

      // populate game
      populateGame:{
        value: function(){
            var cardHolder = this.gameCardContainer.querySelector('ul');

          if(Object.keys(this.cache).length < 3) {

              if(this.score === 0)
                return this.generateLevel({
                  target: cardHolder,
                  cardType: 'bottle-g',
                  colNum: 2,
                  cardNum: 4,
                  flipCards: false
                });

              if(this.score === 1)
                return this.generateLevel({
                  target: cardHolder,
                  cardType: 'miss-mf',
                  colNum: 2,
                  cardNum: 4,
                  flipCards: false
                });

              if(this.score === 2)
                return this.generateLevel({
                  target: cardHolder,
                  cardType: 'plate',
                  colNum: 2,
                  cardNum: 4,
                  flipCards: false
                });

              if(this.score > 2 && this.score <= 4)
                return this.generateLevel({
                  target: cardHolder,
                  cardType: randomCardType(cardTypes, true),
                  colNum: 2,
                  cardNum: 4,
                  flipCards: false
                });
            } else if( this.score <= 4) {
              
              return this.generateLevel({
                  target: cardHolder,
                  cardType: randomCardType(cardTypes, true),
                  colNum: 2,
                  cardNum: 4,
                  flipCards: false
                });

            }

            if(this.score >= 4 && this.score <= 9)
            return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes, true),
              colNum: 3,
              cardNum: 6,
              flipCards: false
            });

            if(this.score >= 9 && this.score <= 12)
            return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes,true),
              colNum: '3-2',
              cardNum: 9,
              flipCards: true
            });

          if(this.score >= 12 && this.score <= 20)
            return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes, true),
              colNum: '3-3',
              cardNum: 12,
              flipCards: true
            });

          if(this.score >= 20 && this.score <= 27 )
            return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes, true),
              colNum: '4',
              cardNum: 16,
              flipCards: true
            });


          if(this.score > 27 && this.height > 500)
            return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes, true),
              colNum: '4',
              cardNum: 20,
              flipCards: true
            });

          return this.generateLevel({
              target: cardHolder,
              cardType: randomCardType(cardTypes, true),
              colNum: '4',
              cardNum: 16,
              flipCards: true
            });
      }
    },

      // next level
      nextLevel: {
        value: function(){
                //clean up previous level
          this.cleanUp();

          // decrease timer
          this.reduceTimer();

          //Add next level
          this.populateGame();

          // move to the nextlevel
          this.transitionNext();

          //Start timer
          this.startTimer();
        },
      },

      // Transition to next level
      transitionNext: {
        value: function(){
            //var style = this.gameCardContainer.style.marginLeft = -Math.abs(this.width * this.score) + 'px';
          var style = this.gameCardContainer.style;

          var transform = 'translateX('+ -Math.abs(this.width * this.score) +'px)';

          style.MozTransform = transform;
          style.WebkitTransform = transform;
          style.OTransform = transform;
        }
      },

      // Started timers
      startTimer: {
        value: function(){
          var self =  this,
              remainingTime = self.baseTimer;

          self.animateTimer(remainingTime);

          this.timer = setInterval(function(){
            //this animate timer
            remainingTime -= 1;
            if(remainingTime < 1){
              clearInterval(self.timer);
              self.gameOver();
            }
          }, 980);
        }
      },


      //Clean up previous level
      cleanUp: {
        value: function(){
          //clear timeout
          clearInterval(this.timer);
            //Reset Level
            this.gameLevelBubble.querySelector('span').innerHTML = this.score;
          //Clear instructions
          if(this.gameInstructions.getAttribute('data-instructions-visible') == 'true')
            this.removeInstructions();

          //Remove classList
          this.gameOverContainer.classList.remove('highscore');
        }
      },

      // game over
      gameOver: {
        value: function(){
          //Clear all sounds
          this.gameOverSounds();

          //Hide highscore issue
          this.gameHighScore.setAttribute('data-isHighScore', false);

          //Display scores
          this.displayGameScore();
          //Show popup
          this.gameOverContainer.setAttribute('data-gmover', true);

          //submit scores to playservices
          this.updateLeaderboardScore();

        }
      },

      updateLeaderboardScore:{
        value: function(){
          var myScore = this.score;
          var data = {
            score: myScore,
            leaderboardId: "CgkIxuvVzboMEAIQAQ"
          };
          googleplaygame.submitScore(data, function(){}, function(){});
        }
      },

      displayGameScore: {
        value: function(){
           //Update best score
          this.updateBestScore();
          //Upate Score
          this.gameYourScore.innerHTML = this.score;
          this.gameBestScore.innerHTML = localStorage['mf-bestscore'];
        }
      },


      gameOverSounds:{
        value: function(){
          stopGameSound();
          generateSound(this.gameSounds.querySelector('.gameoverSound').src, this.soundMuted);
        }
      },

      toggleSound: {
        value: function(){
          var self = this;
          this.soundMuted = !this.soundMuted;

          //toggleIcons
          [].forEach.call(this.gameSoundBubble, function(elem){
            elem.setAttribute('data-sound', !self.soundMuted);
          });

          if(this.soundMuted)
            muteGameSound();
          else
            unmuteGameSound();
        }
      },

      soundMuted: {
        value: false,
        writable: true
      },

      //update score
      updateScore: {
        value: function(){
          var levelTextHolder =  this.gameLevelBubble.querySelector('span');
          this.score += 1;
          levelTextHolder.innerHTML = this.score;
          this.nextLevel();
        }
      },

      updateBestScore: {
        value: function(){
            if(this.score > localStorage['mf-bestscore']){
                localStorage['mf-bestscore'] = this.score;

            this.gameOverContainer.classList.add('highscore');

            this.gameHighScore.setAttribute('data-isHighScore', true);
          }
            return;
        }
      },

      //reduce timer
      reduceTimer: {
        value: function(){
            if(this.baseTimer > this.minTime)
                this.baseTimer -= this.decreaseTime;
            return;
        }
      },

      showLeaderBoard: {
        value: function(){
          var data = {
            leaderboardId: 'CgkIxuvVzboMEAIQAQ'
          };

          googleplaygame.showLeaderboard(data, function(){
            console.log('yay!');
          }, function(){
              alert('woops something went wrong!');
          });

        }
      },

      animateTimer:{
        value: function(time){
            var timer = this.gameTimerBuble.querySelector('.timer-circle');

          // -> removing the class
          timer.classList.remove("timer-animation");
          // -> triggering reflow /* The actual magic */
          // without this it wouldn't work. Try uncommenting the line and the transition won't be retriggered.
          timer.offsetWidth;

          //Add timers
          timer.style.WebkitAnimationDuration = time + "s";  // Code for Chrome, Safari, and Opera
          timer.style.animationDuration = time + "s";

          // -> and re-adding the class
          timer.classList.add("timer-animation");
        }
      },

      removeErrorClass: {
        value: function(elems){
            var lis = elems.querySelectorAll('li');

            for(var i = 0; i < lis.length; i++){
                if(lis[i].classList.contains('error'))
                    lis[i].classList.remove('error');
                else
                    continue;
            }
        }
      },

      //evaluateSelection
      evaluateSelection: {
        value: function(target){
          var tarData = target.getAttribute('data-std');

          if(tarData === 'y'){
            generateSound(this.gameSounds.querySelector('.incorrect').src, this.soundMuted);
            target.classList.add('error');
          } else {
            generateSound(this.gameSounds.querySelector('.correct').src, this.soundMuted);
            this.updateScore();
          }
        }
      },


      shareFacebook: {
        value: function(){
          facebookShare(this.score);
        }
      }

    });

    App.initialize = function(){
      return new Game({
        width: (document.body.offsetWidth < 481)? document.body.offsetWidth : 481,
        height: holderHeight,
        gameContainer: holder,
        gameStartScreen: document.getElementById('startScreen'),
        gameScreen: querySelector('.game-screen'),
        gameOverContainer: document.getElementById('gmov'),
        gameCardContainer: document.getElementById('card-holder'),
        gameInstructions: querySelector('.app-instructions-section'),
        gameLevelBubble: querySelector('.info-circle.level'),
        gameTimerBuble: querySelector('.info-circle.timer'),
        gameSoundBubble: document.querySelectorAll('.info-circle.sound'),
        gameYourScore: querySelector('.yscore .score'),
        gameBestScore: querySelector('.bscore .score'),
        gameHighScore: querySelector('.highscore'),
        gameSounds: querySelector('.sounds'),
        gameMainTune: document.getElementById('gameMainSong'),
        flatBaseTime: 20,
        baseTimer: 20,
        decreaseTime: 1,
        minTime: 4
      });
    };

    return App;


  })(App || {});

